﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace MoviesLib
{
    public partial class AddMovieForm : Form
    {
        /// <summary>
        /// Form responsible for add movie to the database
        /// </summary>
        String name;
        UserPanel parent;

        public AddMovieForm(String userName, UserPanel context)
        {
            /// <summary>
            /// Initialize UI form
            /// </summary>
            InitializeComponent();
            name = userName;
            parent = context;
        }


        private void addMovie(String title, String director, String genre)
        {
            /// <summary>
            /// Adding the movie to the database
            /// </summary>
            XmlDocument doc = new XmlDocument();
            doc.Load("database.xml");
            XmlNodeList xmlUsers = doc.GetElementsByTagName("User");
            XmlElement myXmlUser = null;
            foreach (XmlElement xmlUser in xmlUsers)
            {
                if (xmlUser.GetAttributeNode("name").Value == name)
                {
                    myXmlUser = xmlUser;
                }
            }
            if (myXmlUser != null)
            {
                XmlElement xmlMovie = doc.CreateElement("Movie");
                
                XmlElement xmlTitle = doc.CreateElement("title"); 
                xmlTitle.InnerText = title;

                XmlElement xmlDirector = doc.CreateElement("director");
                xmlDirector.InnerText = director;

                XmlElement xmlGenre = doc.CreateElement("genre");
                xmlGenre.InnerText = genre;

                XmlElement xmlTimesRated = doc.CreateElement("timesRated");
                xmlTimesRated.InnerText = "0";

                XmlElement xmlRating = doc.CreateElement("rating");
                xmlRating.InnerText = "0,0";

                xmlMovie.AppendChild(xmlTitle);
                xmlMovie.AppendChild(xmlDirector);
                xmlMovie.AppendChild(xmlGenre);
                xmlMovie.AppendChild(xmlTimesRated);
                xmlMovie.AppendChild(xmlRating);


                xmlMovie.SetAttribute("id", "movie" + (doc.GetElementsByTagName("Movie").Count + 1));
                myXmlUser.AppendChild(xmlMovie);
                doc.Save("database.xml");
            }
            parent.refresh();
            this.Close();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            /// <summary>
            /// Controller for user action
            /// </summary>
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "")
            {
                addMovie(textBox1.Text, textBox2.Text, textBox3.Text);
            }
            else            
            {
                MessageBox.Show("Please fill all fields");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            /// <summary>
            /// Controller for user action
            /// </summary>
            this.Close();
        }
    }
}
