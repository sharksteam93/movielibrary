﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLib
{
    public class Movie
    {
        /// <summary>
        /// Contain infrormation about a movie.
        /// </summary>
        private String title;
        private String director;
        private String genre;
        private int timesRated;
        private double rating;

        public Movie(String t, String d, String g, int tr, double r)
        {
            title = t; director = d; genre = g; timesRated = tr; rating = r;
        }

        public String Title
        {
            get { return title; }
        }

        public String Director
        {
            get { return director; }
        }
        public String Genre
        {
            get { return genre; }
        }

        public double Rating
        {
            get { return rating; }
        }

        public double TimesRated
        {
            get { return timesRated; }
        }
    }
}
