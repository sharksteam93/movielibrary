﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

namespace MoviesLib
{
    public class Authentication
    {
        /// <summary>
        /// Authenticate user by username and password
        /// </summary>
        public bool authenticate(String name, String password)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("database.xml");
            XmlNodeList xmlUsers = doc.GetElementsByTagName("User");
            bool authenticated = false;
            foreach (XmlNode xmlUser in xmlUsers)
            {
                if (xmlUser.Attributes["name"].InnerText == name && xmlUser.Attributes["password"].InnerText == password)
                {
                    authenticated = true;
                }
            }
            return authenticated;
        } 
    }
}
