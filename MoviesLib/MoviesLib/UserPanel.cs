﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace MoviesLib
{
    public partial class UserPanel : UserControl
    {
        /// <summary>
        /// Contain UI and controls managing the user movies
        /// </summary>

        private formMain main;
        private User user;
        private bool allMovies; // when true - showing all movies, when false - showing user movies
        private List<Movie> movies;
        public UserPanel()
        {
            InitializeComponent();
        }
        public UserPanel(String name, formMain mainForm)
        {
            InitializeComponent();
            labelName.Text = name;
            allMovies = false;
            this.main = mainForm;
            user = new User(name);
            getAllMovies();
            listMovies(user.Movies);
            
            // new user instance here
        }

        public List<Movie> getAllMovies()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("database.xml");
            XmlNodeList xmlMovies = doc.GetElementsByTagName("Movie");
            foreach (XmlElement xmlMovie in xmlMovies)
            {
                Movie movie = new Movie(xmlMovie.GetElementsByTagName("title")[0].InnerText,
                                         xmlMovie.GetElementsByTagName("director")[0].InnerText,
                                         xmlMovie.GetElementsByTagName("genre")[0].InnerText,
                                         int.Parse(xmlMovie.GetElementsByTagName("timesRated")[0].InnerText),
                                         double.Parse(xmlMovie.GetElementsByTagName("rating")[0].InnerText));
                movies.Add(movie);
            }
            return movies;
        }

        private void UserPanel_Load(object sender, EventArgs e)
        {

        }

        private void listMovie_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (allMovies)
            {
                showMovieDesc(listMovie.SelectedIndex, movies);
            }
            else
            {
                showMovieDesc(listMovie.SelectedIndex, user.Movies);
            }
        }

        private void btnMovies_Click(object sender, EventArgs e)
        {
            if (allMovies)
            {
                allMovies = false;
                listMovies(user.Movies);
                (sender as Button).Text = "All movies";
                btnDelete.Visible = true;
            } 
            else
            {
                allMovies = true;
                listMovies(movies);
                (sender as Button).Text = "Your movies";
                btnDelete.Visible = false;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            new AddMovieForm(user.Name, this).Show();
        }

        #region private methods
        private void showMovieDesc(int index, List<Movie> movies)
        {
            if (index >= 0 && index <= movies.Count)
            {
                tbMovieDescription.Text = "Title: " + movies[index].Title +
                                    "\nDirector: " + movies[index].Director +
                                    "\nGenre: " + movies[index].Genre +
                                    "\nRating: " + movies[index].Rating;
            }
            
        }

        private void listMovies(List<Movie> movs)
        {
            listMovie.Items.Clear();
            foreach (Movie mov in movs)
            {
                listMovie.Items.Add(mov.Title);
            }
        }

        private void delete(int index)
        {
            if (index >= 0 && index <= user.Movies.Count)
            {
                deleteFromDatabase(user.Movies[index].Title);
                List<Movie> moviesToDelete = movies.Where(mov => mov.Title == user.Movies[index].Title).ToList();
                Movie movieToDelete = moviesToDelete[0];
                movies.Remove(movieToDelete);
                user.deleteMovie(index);
                listMovies(user.Movies);
            }
        }

        private void deleteFromDatabase(String movieName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("database.xml");
            XmlNodeList xmlMovies = doc.GetElementsByTagName("Movie");
            foreach (XmlElement xmlMovie in xmlMovies)
            {
                if (xmlMovie.GetElementsByTagName("title")[0].InnerText == movieName)
                {
                    xmlMovie.ParentNode.RemoveChild(xmlMovie);
                    doc.Save("database.xml");
                    break;
                }
            }

        }
        #endregion

        #region public methods
        public void refresh()
        {
            movies = new List<Movie>();
            getAllMovies();
            user.getMyMovies();
            if (allMovies)
            {
                listMovies(movies);
            }
            else
            {
                listMovies(user.Movies);
            }
        }
        #endregion

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result1 = MessageBox.Show("Are you sure to delete this movie?", "Delete", MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
            {
                delete(listMovie.SelectedIndex);
            }

            
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            main.logout();
        }


       
    }
}
