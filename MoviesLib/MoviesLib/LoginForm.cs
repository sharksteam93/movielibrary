﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoviesLib
{
    public partial class LoginForm : UserControl
    {
        /// <summary>
        /// Show UI and allows sing in for user
        /// </summary>
        private formMain main;
        private Authentication auth;
        public LoginForm(formMain mainForm)
        {
            InitializeComponent();
            auth = new Authentication();
            this.main = mainForm;
        }


        public void signIn(String name, String password)
        {
            /// <summary>
            /// Authenticating user by name and password
            /// </summary>
            if (auth.authenticate(name, password))
            {
                main.showUserPanel(name, password);
            }
            else
            {
                MessageBox.Show("You have entered invalid credentials");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /// <summary>
            /// Controller for user action
            /// </summary>
            signIn(tbName.Text, tbPassword.Text);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void LoginForm_KeyUp(object sender, KeyEventArgs e)
        {
            /// <summary>
            /// Controller for user action
            /// </summary>
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }

        private void tbPassword_KeyUp(object sender, KeyEventArgs e)
        {
            /// <summary>
            /// Controller for user action
            /// </summary>
            if (e.KeyCode == Keys.Enter)
            {
                signIn(tbName.Text, tbPassword.Text);
            }
        }

        
    }
}
