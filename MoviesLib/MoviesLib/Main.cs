﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoviesLib
{
    public partial class formMain : Form
    {
        /// <summary>
        /// Main application form. Show UI for user.
        /// </summary>
        private UserControl mainControl;
        public formMain()
        {
            InitializeComponent();
            mainControl = new LoginForm(this);

            this.mainPanel.Controls.Add(mainControl);
        }

        private void formMain_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Controls.Remove(mainControl);
        }

        public void showUserPanel(String name, String password)
        {
            this.mainPanel.Controls.Remove(mainControl);
            mainControl = new UserPanel(name, this);
            this.mainPanel.Controls.Add(mainControl);

            this.Refresh();
        }

        public void logout()
        {
            this.mainPanel.Controls.Remove(mainControl);
            mainControl = new LoginForm(this);
            this.mainPanel.Controls.Add(mainControl);
            this.Refresh();
        }
    }
}
