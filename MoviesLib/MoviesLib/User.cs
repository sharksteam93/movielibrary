﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;


namespace MoviesLib
{
    class User
    {
        /// <summary>
        /// Contain information about an user.
        /// </summary>
        private String name;
        private List<Movie> movies;

        public User(String name)
        {
            this.name = name;
            getMyMovies();
        }

        public List<Movie> Movies 
        {
            get { return movies; }
        }

        public String Name
        {
            get { return name; }
        }

        public void getMyMovies()
        {
            movies = new List<Movie>();
            XmlDocument doc = new XmlDocument();
            doc.Load("database.xml");
            XmlNodeList xmlUsers = doc.GetElementsByTagName("User");
            XmlElement myXmlUser = null;
            foreach (XmlElement xmlUser in xmlUsers)
            {
                if (xmlUser.GetAttributeNode("name").Value == name)
                {
                    myXmlUser = xmlUser;
                }
            }
            if (myXmlUser != null)
            {
                XmlNodeList xmlMovies = myXmlUser.GetElementsByTagName("Movie");
                foreach (XmlElement xmlMovie in xmlMovies)
                {
                    movies.Add(new Movie(xmlMovie.GetElementsByTagName("title")[0].InnerText,
                                         xmlMovie.GetElementsByTagName("director")[0].InnerText,
                                         xmlMovie.GetElementsByTagName("genre")[0].InnerText,
                                         int.Parse(xmlMovie.GetElementsByTagName("timesRated")[0].InnerText),
                                         double.Parse(xmlMovie.GetElementsByTagName("rating")[0].InnerText)));
                }
            }
        }

        public void deleteMovie(int index)
        {
            movies.RemoveAt(index);
        }
    }
}
