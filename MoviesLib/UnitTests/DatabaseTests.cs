﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using System.IO;
using System.Collections.Generic;

namespace MoviesLib
{
    [TestClass]
    public class DatabaseTests
    {
        [TestMethod]
        public void CreatingEmptyDB()
        {
            string dbFile = "test_db.xml";

            // Create the XmlDocument.
            XmlDocument doc = new XmlDocument();
            doc.LoadXml("<Database></Database>");

            // Save the document to a file and auto-indent the output.
            XmlTextWriter writer = new XmlTextWriter(dbFile, null);
            writer.Formatting = Formatting.Indented;
            doc.Save(writer);
            writer.Close();

            Assert.AreEqual(true, File.Exists(dbFile));

            using (StreamReader sr = new StreamReader(dbFile, true))
            {
                Console.WriteLine(sr.ReadToEnd());
            }

            File.Delete(dbFile);
        }

        [TestMethod]
        public void LoginUser()
        {
            Authentication a = new Authentication();
            Assert.AreEqual(true, a.authenticate("Adrian", "test"));
            Assert.AreEqual(true, a.authenticate("Lukasz", "test"));
            Assert.AreEqual(true, a.authenticate("Jan", "test"));
            Assert.AreEqual(true, a.authenticate("Egon", "test"));
        }

        [TestMethod]
        public void ReadingUserMovies()
        {
            UserPanel up = new UserPanel();
            List<Movie> movies = up.getAllMovies();

            Console.WriteLine(movies.Count);
            foreach (Movie m in movies)
            {
                Console.WriteLine(m.Title + " " + m.Director + " " + m.Genre);
            }

            Assert.AreEqual(12, movies.Count);
            Assert.AreEqual("The Shawshank Redemption", movies[0].Title);
            Assert.AreEqual("Frank Darabont", movies[0].Director);
            Assert.AreEqual("The Lion King", movies[11].Title);
            Assert.AreEqual("Animation", movies[11].Genre);
        }
    }
}
